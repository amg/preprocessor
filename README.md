<a name="logo"/>
<div align="center">
<a href="https://www.dynare.org/" target="_blank">
<img src="https://www.dynare.org/assets/images/logo/dlogo.svg" alt="Dynare Logo"></img>
</a>
</div>

# Dynare Preprocessor

The Dynare Preprocessor defines the Dynare model language. It takes in a `.mod`
file, computes the derivatives of the model represented therein, and produces
MATLAB/Octave, C, Julia, or JSON output.

There is more to come here. For the moment, see the [Dynare
repository](https://git.dynare.org/Dynare/dynare)

# License

Most of the source files are covered by the GNU General Public Licence version
3 or later. There are some exceptions. See [license.txt](license.txt) for specifics.
